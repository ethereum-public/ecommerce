// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

import { Ownable } from "@openzeppelin/contracts/access/Ownable.sol";
import { Pausable } from "@openzeppelin/contracts/security/Pausable.sol";
import { EnumerableSet } from "@openzeppelin/contracts/utils/structs/EnumerableSet.sol";

struct Order {
    string product;         // product name
    uint256 amount;         // product amount
    uint256 price;          // product price
    address beneficiary;    // order beneficiary
    OrderStatus status;     // current status
}

enum OrderStatus {
    DELETED,                // = 0
    CREATED,                // = 1
    PAID,                   // = 2
    SHIPPED,                // = 3
    DEVLIVERED              // = 4
}

contract Ecommerce is Ownable, Pausable {

    using EnumerableSet for EnumerableSet.AddressSet;

    EnumerableSet.AddressSet internal _admins;

    mapping(bytes32 id => Order) internal _orders;

    /***********
     * Errors
     **********/

    /// Invalid product name
    error InvalidProductName();

    /// Invalid price
    error InvalidPrice();

    /// Invalid amount
    error InvalidAmount();

    /// Order with this id already exists
    error OrderAlreadyExists(bytes32 id);

    /// Order with this id does not exist
    error OrderDoesNotExist(bytes32 id);

    /// Order can no longer be modified
    error ImmutableOrder(bytes32 id);

    /// Action can only by executed by an admin/owner
    error UnauthorizedAdmin();

    /// Action can only by executed by an admin/owner or the order's beneficiary
    error UnauthorizedAdminOrBeneficiary();

    /// Status is out of scope
    error UnsupportedStatus(uint256 status);

    /// Status does not match the existing status
    error MismatchedStatus(uint256 status);

    /***********
     * Events
     **********/

    event AdminAdded(
        address account
    );

    event AdminRemoved(
        address account
    );

    event OrderCreated(
        bytes32 indexed id,
        string product,
        uint256 amount,
        uint256 price,
        address beneficiary
    );

    event OrderDataUpdate(
        bytes32 indexed id,
        string product,
        uint256 amount,
        uint256 price
    );

    event OrderStatusUpdate(
        bytes32 indexed id,
        uint256 status
    );

    /***********
     * Modifiers
     **********/

    modifier onlyNonExistingOrder(bytes32 id) {
        if (_orderExists(id)) {
            revert OrderAlreadyExists(id);
        }
        _;
    }

    modifier onlyExistingOrder(bytes32 id) {
        if (!_orderExists(id)
            || _orders[id].status == OrderStatus.DELETED) {
            revert OrderDoesNotExist(id);
        }
        _;
    }

    modifier onlyMutableOrder(bytes32 id) {
        if (_orders[id].status > OrderStatus.CREATED) {
            revert ImmutableOrder(id);
        }
        _;
    }

    modifier onlyAdmin() {
        if (!_isAdmin(msg.sender)) {
            revert UnauthorizedAdmin();
        }
        _;
    }

    modifier onlyAdminOrBeneficiary(bytes32 id) {
        if (!_isAdmin(msg.sender)
            && _orders[id].beneficiary != msg.sender) {
            revert UnauthorizedAdminOrBeneficiary();
        }
        _;
    }

    /**
     * @param initialOwner initial onwer of the contract, if zero-address is supplied the ownership remains allocated to the deployer's address
     */
    constructor(address initialOwner) {
        if (initialOwner != address(0)) {
            _transferOwnership(initialOwner);
        }
    }

    /**
     * Creates a new order
     * Anyone is allowed to execute this method
     * The beneficiary is set to the account that has invoked this method
     * Execution only allowed when contract is not paused
     * @param id order id
     * @param product product name, any non-empty string, not more than 32 characters long
     * @param amount product amount, any positive integer
     * @param price product price, any positive integer expressed in the wei convention: https://etherscan.io/unitconverter
     */
    function createOrder(
        bytes32 id,
        string memory product,
        uint256 amount,
        uint256 price
    )
        external whenNotPaused onlyNonExistingOrder(id)
    {
        _validateProduct(product);
        _validateAmount(amount);
        _validatePrice(price);
        address beneficiary = msg.sender;
        _orders[id] = Order(product, amount, price, beneficiary, OrderStatus.CREATED);
        emit OrderCreated(id, product, amount, price, beneficiary);
    }

    /**
     * Updates the status of an existing order
     * Order can only be deleted or upgraded to a higher status
     * Execution only allowed for admins or the contract owner
     * Execution only allowed when contract is not paused
     * @param id order id
     * @param status new order status, use integer numbers as described in the `OrderStatus` enumeration
     */
    function updateOrder(
        bytes32 id,
        uint256 status
    )
        external whenNotPaused onlyExistingOrder(id) onlyAdmin
    {
        if (status > uint256(type(OrderStatus).max)) {
            revert UnsupportedStatus(status);
        }
        if (status > 0 && status <= uint256(_orders[id].status)) {
            revert MismatchedStatus(status);
        }
        _orders[id].status = OrderStatus(status);
        emit OrderStatusUpdate(id, status);
    }

    /**
     * Updates the data of an existing order
     * Pass an empty value (i.e. 0 or "") for fields that should not be modified
     * Execution only allowed for admins, the contract owner or the order's beneficiary
     * Execution only allowed when contract is not paused
     * @param id order id
     * @param product new product name, apply an empty string to keep the current value
     * @param amount new product amount, apply zero to keep the current value
     * @param price new product price, apply zero to keep the current value
     */
    function updateOrder(
        bytes32 id,
        string memory product,
        uint256 amount,
        uint256 price
    )
        external whenNotPaused onlyExistingOrder(id) onlyMutableOrder(id) onlyAdminOrBeneficiary(id)
    {
        Order storage order = _orders[id];
        if (bytes(product).length > 0) {
            _validateProduct(product);
            order.product = product;
        }
        if (amount > 0) {
            _validateAmount(amount);
            order.amount = amount;
        }
        if (price > 0) {
            _validatePrice(price);
            order.price = price;
        }
        emit OrderDataUpdate(id, product, amount, price);
    }

    /**
     * Retrieves an existing order, even if it's been deleted
     * Returns empty data if an order never existed
     * @param id order id
     * @return product product name
     * @return amount product amount
     * @return price product price
     * @return beneficiary order beneficiary
     * @return status order status, refer to the `OrderStatus` enumeration for decoding integer values
     */
    function retrieveOrder(bytes32 id)
        external view returns(
            string memory product,
            uint256 amount,
            uint256 price,
            address beneficiary,
            uint256 status
        )
    {
        Order memory order = _orders[id];
        return (order.product, order.amount, order.price, order.beneficiary, uint256(order.status));
    }
    
    /**
     * Pauses the contract
     * Only the contract owner is allowed to execute this method
     */
    function pause()
        external onlyOwner
    {
        _pause();
    }

    /**
     * Unpauses the contract
     * Only the contract owner is allowed to execute this method
     */
    function unpause()
        external onlyOwner
    {
        _unpause();
    }

    /**
     * Adds a new admin account
     * Only the contract owner is allowed to execute this method
     * @param account account to be added
     */
    function addAdmin(address account)
        external onlyOwner
    {
        if (_admins.add(account)) {
            emit AdminAdded(account);
        }
    }

    /**
     * Removes an existing admin account
     * Only the contract owner is allowed to execute this method
     * @param account account to be removed
     */
    function removeAdmin(address account)
        external onlyOwner
    {
        if (_admins.remove(account)) {
            emit AdminRemoved(account);
        }
    }

    /**
     * Retrieves the list of all active admin accounts
     */
    function admins()
        external view returns(address[] memory)
    {
        return _admins.values();
    }

    /**
     * @dev Verify if an order already exists in the mapping
     * @notice Based on the assumption that an existing order cannot have zero as an amount
     */
    function _orderExists(bytes32 id)
        internal view returns(bool)
    {
        return _orders[id].amount > 0;
    }

    /**
     * @dev Verify if an account belongs to the admin list
     * @notice The contract owner is treated as an admin
     */
    function _isAdmin(address account)
        internal view returns(bool)
    {
        return account == owner() || _admins.contains(account);
    }

    /**
     * @dev Validate product name
     */
    function _validateProduct(string memory product)
        internal pure
    {
        if (bytes(product).length == 0 || bytes(product).length > 32) {
            revert InvalidProductName();
        }
    }

    /**
     * @dev Validate product amount
     */
    function _validateAmount(uint256 amount)
        internal pure
    {
        if (amount == 0) {
            revert InvalidAmount();
        }
    }

    /**
     * @dev Validate product price
     */
    function _validatePrice(uint256 price)
        internal pure
    {
        if (price == 0) {
            revert InvalidPrice();
        }
    }
    
}
