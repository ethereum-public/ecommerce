import { task } from "hardhat/config";
import { Ecommerce } from "./typechain-types";

task("deploy")
    .addPositionalParam("owner")
    .setAction(async (taskArgs, hre) => {
        console.log("Deploying Ecommerce");

        const owner: string = taskArgs["owner"];
        const factory = await hre.ethers.getContractFactory("Ecommerce");
        const ecommerce: Ecommerce = await factory.deploy(owner);

        await ecommerce.deployed();
        console.log(`DEPLOYED Ecommerce at ${ecommerce.address}`);

        if (hre.network.name === "sepolia") {
            await ecommerce.deployTransaction.wait(3);
            await hre.run("verify:verify", {
                address: ecommerce.address,
                constructorArguments: [owner]
            });
        }
    });
