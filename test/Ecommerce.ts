import { loadFixture } from "@nomicfoundation/hardhat-network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Ecommerce", function () {

    const ADDRESS_ZERO = "0x0000000000000000000000000000000000000000";

    const ORDER_STATUS = { DELETED: 0, CREATED: 1, PAID: 2, SHIPPED: 3, DEVLIVERED: 4 };

    async function deployEcommerceFixture() {

        const order1 = { id: ethers.utils.hexlify(ethers.utils.randomBytes(32)), product: "Product A", amount: 44, price: 188229 };
        const order2 = { id: ethers.utils.hexlify(ethers.utils.randomBytes(32)), product: "Product B", amount: 69, price: 2020 };
        const order3 = { id: ethers.utils.hexlify(ethers.utils.randomBytes(32)), product: "Product A", amount: 12, price: 8373823 };
        const order4 = { id: ethers.utils.hexlify(ethers.utils.randomBytes(32)), product: "Product C", amount: 1, price: 232 };

        const [deployer, owner, admin1, admin2, user1, user2] = await ethers.getSigners();

        const Ecommerce = await ethers.getContractFactory("Ecommerce");
        const ecommerce = await Ecommerce.deploy(owner.address);

        await ecommerce.connect(owner).addAdmin(admin1.address);
        await ecommerce.connect(owner).addAdmin(admin2.address);

        await ecommerce.connect(user1).createOrder(order1.id, order1.product, order1.amount, order1.price);
        await ecommerce.connect(user2).createOrder(order2.id, order2.product, order2.amount, order2.price);

        return { ecommerce, deployer, owner, admin1, admin2, user1, user2, order1, order2, order3, order4 };
    }

    describe("Contract deployment", function () {
        it("Should set the right owner", async function () {
            const { ecommerce, owner } = await loadFixture(deployEcommerceFixture);

            expect(await ecommerce.owner()).to.equal(owner.address);
        });

        it("Should set the right admins", async function () {
            const { ecommerce, admin1, admin2 } = await loadFixture(deployEcommerceFixture);

            const admins = await ecommerce.admins();
            expect(admins.length).to.equal(2);
            expect(admins[0]).to.equal(admin1.address);
            expect(admins[1]).to.equal(admin2.address);
        });
    });

    describe("Contract pausing", function () {
        it("Should apply pause and unpause", async function () {
            const { ecommerce, owner } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(owner).pause();
            expect(await ecommerce.paused()).to.equal(true);

            await ecommerce.connect(owner).unpause();
            expect(await ecommerce.paused()).to.equal(false);
        });

        it("Should fail if invoked by a non-owner", async function () {
            const { ecommerce, admin1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(admin1).pause())
                .to.be.revertedWith("Ownable: caller is not the owner");
        });
    });

    describe("Admin management", function () {
        it("Should correctly add an admin and emit appropriate event", async function () {
            const { ecommerce, owner, admin1, admin2, user1 } = await loadFixture(deployEcommerceFixture);

            await expect(ecommerce.connect(owner).addAdmin(user1.address))
                .to.emit(ecommerce, "AdminAdded")
                .withArgs(user1.address);

            const admins = await ecommerce.admins();
            expect(admins.length).to.equal(3);
            expect(admins[0]).to.equal(admin1.address);
            expect(admins[1]).to.equal(admin2.address);
            expect(admins[2]).to.equal(user1.address);
        });

        it("Should correctly remove an admin and emit appropriate event", async function () {
            const { ecommerce, owner, admin1, admin2 } = await loadFixture(deployEcommerceFixture);

            await expect(ecommerce.connect(owner).removeAdmin(admin1.address))
                .to.emit(ecommerce, "AdminRemoved")
                .withArgs(admin1.address);

            const admins = await ecommerce.admins();
            expect(admins.length).to.equal(1);
            expect(admins[0]).to.equal(admin2.address);
        });

        it("Should fail if invoked by a non-owner", async function () {
            const { ecommerce, admin1, admin2, user1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(admin1).addAdmin(user1.address))
                .to.be.revertedWith("Ownable: caller is not the owner");

            await expect(ecommerce.connect(admin1).removeAdmin(admin2.address))
                .to.be.revertedWith("Ownable: caller is not the owner");
        });
    });

    describe("Creating an order", function () {
        it("Should pass if invoked by anyone", async function () {
            const { ecommerce, user1, user2, order3, order4 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1).createOrder(order3.id, order3.product, order3.amount, order3.price))
                .not.to.be.reverted;

            await expect(ecommerce.connect(user2).createOrder(order4.id, order4.product, order4.amount, order4.price))
                .not.to.be.reverted;
        });

        it("Should emit appropriate event", async function () {
            const { ecommerce, user1, user2, order3, order4 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1).createOrder(order3.id, order3.product, order3.amount, order3.price))
                .to.emit(ecommerce, "OrderCreated")
                .withArgs(order3.id, order3.product, order3.amount, order3.price, user1.address);

            await expect(ecommerce.connect(user2).createOrder(order4.id, order4.product, order4.amount, order4.price))
                .to.emit(ecommerce, "OrderCreated")
                .withArgs(order4.id, order4.product, order4.amount, order4.price, user2.address);
        });

        it("Should fail if contract is paused", async function () {
            const { ecommerce, owner, user1, order3 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(owner).pause();
            
            await expect(ecommerce.connect(user1).createOrder(order3.id, order3.product, order3.amount, order3.price))
                .to.be.revertedWith("Pausable: paused");
        });

        it("Should fail if supplied with an invalid input", async function () {
            const { ecommerce, user1, order3 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1).createOrder(order3.id, "", order3.amount, order3.price))
                .to.be.revertedWithCustomError(ecommerce, "InvalidProductName");
            
            await expect(ecommerce.connect(user1).createOrder(order3.id, "Product name with significantly more then thirty two characters", order3.amount, order3.price))
                .to.be.revertedWithCustomError(ecommerce, "InvalidProductName");

            await expect(ecommerce.connect(user1).createOrder(order3.id, order3.product, 0, order3.price))
                .to.be.revertedWithCustomError(ecommerce, "InvalidAmount");

            await expect(ecommerce.connect(user1).createOrder(order3.id, order3.product, order3.amount, 0))
                .to.be.revertedWithCustomError(ecommerce, "InvalidPrice");
        });

        it("Should fail if order already exists", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1).createOrder(order1.id, order1.product, order1.amount, order1.price))
                .to.be.revertedWithCustomError(ecommerce, "OrderAlreadyExists");
        });

        it("Should apply the correct beneficiary and initial status", async function () {
            const { ecommerce, user1, user2, order3, order4 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(user1).createOrder(order3.id, order3.product, order3.amount, order3.price);
            await ecommerce.connect(user2).createOrder(order4.id, order4.product, order4.amount, order4.price);
            
            {
                const { beneficiary, status } = await ecommerce.retrieveOrder(order3.id);
                expect(beneficiary).to.equal(user1.address);
                expect(status).to.equal(ORDER_STATUS.CREATED);
            }
            {
                const { beneficiary, status } = await ecommerce.retrieveOrder(order4.id);
                expect(beneficiary).to.equal(user2.address);
                expect(status).to.equal(ORDER_STATUS.CREATED);
            }
        });
    });

    describe("Retrieving an order", function () {
        
        it("Should retrieve an existing order", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            const { product, amount, price, beneficiary, status } = await ecommerce.retrieveOrder(order1.id);
            expect(product).to.equal(order1.product);
            expect(amount).to.equal(order1.amount);
            expect(price).to.equal(order1.price);
            expect(beneficiary).to.equal(user1.address);
            expect(status).to.equal(ORDER_STATUS.CREATED);
        });

        it("Should retrieve a deleted order", async function () {
            const { ecommerce, admin1, user1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED);
            
            const { product, amount, price, beneficiary, status } = await ecommerce.retrieveOrder(order1.id);
            expect(product).to.equal(order1.product);
            expect(amount).to.equal(order1.amount);
            expect(price).to.equal(order1.price);
            expect(beneficiary).to.equal(user1.address);
            expect(status).to.equal(ORDER_STATUS.DELETED);
        });

        it("Should retrieve empty data for a non-existing order", async function () {
            const { ecommerce } = await loadFixture(deployEcommerceFixture);
            
            const { product, amount, price, beneficiary, status } = await ecommerce.retrieveOrder(ethers.utils.hexlify(ethers.utils.randomBytes(32)));
            expect(product).to.equal("");
            expect(amount).to.equal(0);
            expect(price).to.equal(0);
            expect(beneficiary).to.equal(ADDRESS_ZERO);
            expect(status).to.equal(ORDER_STATUS.DELETED);
        });
    });

    describe("Updating order status", function () {
        it("Should pass if invoked by an authorized user: owner or admin", async function () {
            const { ecommerce, owner, admin1, admin2, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(owner)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .not.to.be.reverted;
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.SHIPPED))
                .not.to.be.reverted;

            await expect(ecommerce.connect(admin2)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DEVLIVERED))
                .not.to.be.reverted;
        });

        it("Should emit appropriate event", async function () {
            const { ecommerce, owner, admin1, admin2, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(owner)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .to.emit(ecommerce, "OrderStatusUpdate")
                .withArgs(order1.id, ORDER_STATUS.PAID);

            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.SHIPPED))
                .to.emit(ecommerce, "OrderStatusUpdate")
                .withArgs(order1.id, ORDER_STATUS.SHIPPED);

            await expect(ecommerce.connect(admin2)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DEVLIVERED))
                .to.emit(ecommerce, "OrderStatusUpdate")
                .withArgs(order1.id, ORDER_STATUS.DEVLIVERED);
        });

        it("Should allow to jump to any status above the current one", async function () {
            const { ecommerce, owner, admin1, admin2, order1 } = await loadFixture(deployEcommerceFixture);
            
            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID);

            await expect(ecommerce.connect(admin2)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DEVLIVERED))
                .not.to.be.reverted;
        });

        it("Should allow to delete an order at any stage", async function () {
            const { ecommerce, admin1, admin2, order1 } = await loadFixture(deployEcommerceFixture);
            
            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID);
            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.SHIPPED);

            await expect(ecommerce.connect(admin2)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED))
                .not.to.be.reverted;
        });

        it("Should fail if contract is paused", async function () {
            const { ecommerce, owner, admin1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(owner).pause();
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .to.be.revertedWith("Pausable: paused");
        });

        it("Should fail if invoked by an unauthorized user", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .to.be.revertedWithCustomError(ecommerce, "UnauthorizedAdmin");
        });

        it("Should fail if the new status is out of scope", async function () {
            const { ecommerce, admin1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, 5))
                .to.be.revertedWithCustomError(ecommerce, "UnsupportedStatus");
        });

        it("Should fail if the new status does not match the existing one", async function () {
            const { ecommerce, admin1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID);
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.CREATED))
                .to.be.revertedWithCustomError(ecommerce, "MismatchedStatus");
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .to.be.revertedWithCustomError(ecommerce, "MismatchedStatus");
        });

        it("Should fail at an attempt to delete a deleted order", async function () {
            const { ecommerce, admin1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED);
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED))
                .to.be.revertedWithCustomError(ecommerce, "OrderDoesNotExist");
        });

        it("Should fail at an attempt to change the status of a deleted order", async function () {
            const { ecommerce, admin1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED);
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID))
                .to.be.revertedWithCustomError(ecommerce, "OrderDoesNotExist");
        });

        it("Should apply the requested change", async function () {
            const { ecommerce, admin1, order1 } = await loadFixture(deployEcommerceFixture);
            {
                await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID);
                const { status } = await ecommerce.retrieveOrder(order1.id);
                expect(status).to.equal(ORDER_STATUS.PAID);
            }

            {
                await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.SHIPPED);
                const { status } = await ecommerce.retrieveOrder(order1.id);
                expect(status).to.equal(ORDER_STATUS.SHIPPED);
            }

            {
                await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DEVLIVERED);
                const { status } = await ecommerce.retrieveOrder(order1.id);
                expect(status).to.equal(ORDER_STATUS.DEVLIVERED);
            }

            {
                await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.DELETED);
                const { status } = await ecommerce.retrieveOrder(order1.id);
                expect(status).to.equal(ORDER_STATUS.DELETED);
            }
        });
    });

    describe("Updating order data", function () {
        it("Should pass if invoked by an authorized user: owner, admin or beneficiary", async function () {
            const { ecommerce, owner, admin1, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(owner)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 0))
                .not.to.be.reverted;
            
            await expect(ecommerce.connect(admin1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "Product X", 43, 0))
                .not.to.be.reverted;

            await expect(ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 1876))
                .not.to.be.reverted;
        });

        it("Should emit appropriate event", async function () {
            const { ecommerce, owner, admin1, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(owner)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 0))
                .to.emit(ecommerce, "OrderDataUpdate")
                .withArgs(order1.id, "", order1.amount + 1, 0);
        });

        it("Should fail if contract is paused", async function () {
            const { ecommerce, owner, user1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(owner).pause();
            
            await expect(ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 0))
                .to.be.revertedWith("Pausable: paused");
        });

        it("Should fail if invoked by an unauthorized user", async function () {
            const { ecommerce, user2, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user2)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 0))
                .to.be.revertedWithCustomError(ecommerce, "UnauthorizedAdminOrBeneficiary");
        });

        it("Should fail if incorrect product name is supplied", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await expect(ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "Product name with significantly more then thirty two characters", 0, 0))
                .to.be.revertedWithCustomError(ecommerce, "InvalidProductName");
        });

        it("Should fail if invoked after payment is made", async function () {
            const { ecommerce, admin1, user1, order1 } = await loadFixture(deployEcommerceFixture);

            await ecommerce.connect(admin1)["updateOrder(bytes32,uint256)"](order1.id, ORDER_STATUS.PAID);
            
            await expect(ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", order1.amount + 1, 0))
                .to.be.revertedWithCustomError(ecommerce, "ImmutableOrder");
        });

        it("Should apply the requested change", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            const newProduct = "Product X";
            const newAmount = 13;
            const newPrice = 76323;

            await ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, newProduct, 0, 0);
            
            const { product } = await ecommerce.retrieveOrder(order1.id);
            expect(product).to.equal(newProduct);

            await ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", newAmount, 0);
            
            const { amount } = await ecommerce.retrieveOrder(order1.id);
            expect(amount).to.equal(newAmount);

            await ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", 0, newPrice);
            
            const { price } = await ecommerce.retrieveOrder(order1.id);
            expect(price).to.equal(newPrice);
        });

        it("Should preserve existing values", async function () {
            const { ecommerce, user1, order1 } = await loadFixture(deployEcommerceFixture);
            
            await ecommerce.connect(user1)["updateOrder(bytes32,string,uint256,uint256)"](order1.id, "", 0, 0);
            
            const { product, amount, price } = await ecommerce.retrieveOrder(order1.id);
            expect(product).to.equal(order1.product);
            expect(amount).to.equal(order1.amount);
            expect(price).to.equal(order1.price);
        });
    });
});
