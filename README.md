# Toolset
* Solidity: `v0.8.19`
* DevOps: `Hardhat`
* Testing: `Ethers` & `TypeScript`
* Testnet: `Sepolia`

# Contract
The source code is located in this file:
```
./contracts/Ecommerce.sol
```

The contract extends two basic OpenZeppelin contracts:
* `Ownable` - to support ownership management of the contract
* `Pausable` - to enable pausing and unpausing for some of the contract's methods

Here is the main data structures used in the contract:
```
struct Order {
    string product;         // product name
    uint256 amount;         // product amount
    uint256 price;          // product price
    address beneficiary;    // order beneficiary
    OrderStatus status;     // current status
}
```
```
enum OrderStatus {
    DELETED,                // = 0
    CREATED,                // = 1
    PAID,                   // = 2
    SHIPPED,                // = 3
    DEVLIVERED              // = 4
}
```
## Method `createOrder`
Creates a new order:
* Anyone is allowed to execute this method
* The beneficiary is set to the account that has invoked this method
* Execution only allowed when contract is not paused

Signature:
```
function createOrder(
    bytes32 id,
    string memory product,
    uint256 amount,
    uint256 price
)
```
Arguments:
* `id` - order id
* `product` - product name, any non-empty string, not more than 32 characters long
* `amount` - product amount, any positive integer
* `price` - product price, any positive integer expressed in the wei convention: https://etherscan.io/unitconverter

## Method `updateOrder` (for updating order status)
Updates the status of an existing order:
* Order can only be deleted or upgraded to a higher status
* Execution only allowed for admins or the contract owner
* Execution only allowed when contract is not paused

Signature:
```
function updateOrder(
    bytes32 id,
    uint256 status
)
```
Arguments:
* `id` - order id
* `status` - new order status, use integer numbers as described in the `OrderStatus` enumeration

## Method `updateOrder` (for updating order data)
Updates the data of an existing order:
* Pass an empty value (i.e. 0 or "") for fields that should not be modified
* Execution only allowed for admins, the contract owner or the order's beneficiary
* Execution only allowed when contract is not paused

Signature:
```
function updateOrder(
    bytes32 id,
    string memory product,
    uint256 amount,
    uint256 price
)
```
Arguments:
* `id` - order id
* `product` - new product name, apply an empty string to keep the current value
* `amount` - new product amount, apply zero to keep the current value
* `price` - new product price, apply zero to keep the current value

## Method `retrieveOrder`
Retrieves an existing order, even if it's been deleted. Returns empty data if an order never existed.

Signature:
```
function retrieveOrder(bytes32 id)
    external view returns(
        string memory product,
        uint256 amount,
        uint256 price,
        address beneficiary,
        uint256 status
    )
```
Arguments:
* `id` - order id

Returns:
* `product` product name
* `amount` product amount
* `price` product price
* `beneficiary` order beneficiary
* `status` order status, refer to the `OrderStatus` enumeration for decoding integer values

## Other methods:
* `transferOwnership(address account)` - transfers ownership of the contract to a new account (only owner)
* `pause()` - pauses the contract (only owner)
* `unpause()` - unpauses the contract (only owner)
* `addAdmin(address account)`- adds a new admin account (only owner)
* `removeAdmin(address account)` - removes an existing admin account (only owner)
* `admins()` - retrieves the list of all active admin accounts

# Install
```
npm install
```
# Compile
To compile the Solidity source code run:
```
npx hardhat clean
npx hardhat compile
```
Expected output:
```
Generating typings for: 5 artifacts in dir: typechain-types for target: ethers-v5
Successfully generated 20 typings!
Compiled 5 Solidity files successfully
```

# Test
All unit-tests are located in this file:
```
./test/Ecommerce.ts
```

To execute unit-tests run:
```
npx hardhat test
```
Expected output:
```
  Ecommerce
    Contract deployment
      ✔ Should set the right owner (15457ms)
      ✔ Should set the right admins (75ms)
    Contract pausing
      ✔ Should apply pause and unpause (100ms)
      ✔ Should fail if invoked by a non-owner (71ms)
    Admin management
      ✔ Should correctly add an admin and emit appropriate event (134ms)
      ✔ Should correctly remove an admin and emit appropriate event (93ms)
      ✔ Should fail if invoked by a non-owner (40ms)
    Creating an order
      ✔ Should pass if invoked by anyone (81ms)
      ✔ Should emit appropriate event (183ms)
      ✔ Should fail if contract is paused (43ms)
      ✔ Should fail if supplied with an invalid input (67ms)
      ✔ Should fail if order already exists
      ✔ Should apply the correct beneficiary and initial status (108ms)
    Retrieving an order
      ✔ Should retrieve an existing order (58ms)
      ✔ Should retrieve a deleted order (75ms)
      ✔ Should retrieve empty data for a non-existing order (54ms)
    Updating order status
      ✔ Should pass if invoked by an authorized user: owner or admin (72ms)
      ✔ Should emit appropriate event (117ms)
      ✔ Should allow to jump to any status above the current one (43ms)
      ✔ Should allow to delete an order at any stage (59ms)
      ✔ Should fail if contract is paused (42ms)
      ✔ Should fail if invoked by an unauthorized user
      ✔ Should fail if the new status is out of scope
      ✔ Should fail if the new status does not match the existing one (80ms)
      ✔ Should fail at an attempt to delete a deleted order
      ✔ Should fail at an attempt to change the status of a deleted order
      ✔ Should apply the requested change (92ms)
    Updating order data
      ✔ Should pass if invoked by an authorized user: owner, admin or beneficiary (64ms)
      ✔ Should emit appropriate event (68ms)
      ✔ Should fail if contract is paused
      ✔ Should fail if invoked by an unauthorized user
      ✔ Should fail if incorrect product name is supplied
      ✔ Should fail if invoked after payment is made
      ✔ Should apply the requested change (92ms)
      ✔ Should preserve existing values (47ms)

  35 passing (18s)
```

To execute a unit-test with a gas estimation run:
```
REPORT_GAS=true npx hardhat test
```
Expected output:
```
·-----------------------------|---------------------------|-------------|-----------------------------·
|    Solc version: 0.8.19     ·  Optimizer enabled: true  ·  Runs: 200  ·  Block limit: 30000000 gas  │
······························|···························|·············|······························
|  Methods                                                                                            │
··············|···············|·············|·············|·············|···············|··············
|  Contract   ·  Method       ·  Min        ·  Max        ·  Avg        ·  # calls      ·  usd (avg)  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  addAdmin     ·      73630  ·      90742  ·      79338  ·            3  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  createOrder  ·     114726  ·     114750  ·     114740  ·            8  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  pause        ·          -  ·          -  ·      27714  ·            4  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  removeAdmin  ·          -  ·          -  ·      40481  ·            1  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  unpause      ·          -  ·          -  ·      27685  ·            1  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  updateOrder  ·      32348  ·      34609  ·      34334  ·           22  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Ecommerce  ·  updateOrder  ·      32475  ·      38120  ·      36995  ·           10  ·          -  │
··············|···············|·············|·············|·············|···············|··············
|  Deployments                ·                                         ·  % of limit   ·             │
······························|·············|·············|·············|···············|··············
|  Ecommerce                  ·          -  ·          -  ·     975151  ·        3.3 %  ·          -  │
·-----------------------------|-------------|-------------|-------------|---------------|-------------·
```

# Deploy
The deployment script is located in this file:
```
./tasks.ts
```

The deplyment script takes one agrument, as it is required by the contract's constructor: the address of the initial owner of the contract.

If a zero address is provided for the constructor argument (i.e. `0x0000000000000000000000000000000000000000`), the ownership remains allocated to the deployer's address.

## Hardhat network
To deploy on a Hardhat (non-persistent) network run:
```
npx hardhat deploy 0x0000000000000000000000000000000000000000
```
Expected output:
```
Deploying Ecommerce
DEPLOYED Ecommerce at 0x5FbDB2315678afecb367f032d93F642f64180aa3
```

## Local network
To deploy on a local (persistent) network, make sure that the network in running on your local machine.

To launch a local network run:
```
npx hardhat node
```
To deploy on a local network run:
```
npx hardhat --network localhost deploy 0x0000000000000000000000000000000000000000
```

## Public testnet
Make sure you have the following environment variables properly configured:
```
ETHERSCAN_API_KEY = "XXXXXXXXXXXXXXXXXXXXXXXXXX"
INFURA_API_KEY = "YYYYYYYYYYYYYYYYYYYYYYYYYYYY"
PRIVATE_KEY = "ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ"
```

To deploy on the Sepolia testnet run:
```
npx hardhat --network sepolia deploy 0x0000000000000000000000000000000000000000
```
> Note: When deploying on a public testnet you might want to replace `0x0000000000000000000000000000000000000000` with an actual address of one of your accounts in order to gain ownership of the contract.

The deployment script not only deploys the contract on Sepolia, but it also verifies and publishes the source code on Sepolia's etherscan.

Expected output:
```
Deploying Ecommerce
DEPLOYED Ecommerce at 0xa626578A62824dD4b3259424c329F764F2E08965
Nothing to compile
No need to generate any newer typings.
Successfully submitted source code for contract
contracts/Ecommerce.sol:Ecommerce at 0xa626578A62824dD4b3259424c329F764F2E08965
for verification on the block explorer. Waiting for verification result...

Successfully verified contract Ecommerce on Etherscan.
https://sepolia.etherscan.io/address/0xa626578A62824dD4b3259424c329F764F2E08965#code
```

Use this URL to interact with the deployed contract:

https://sepolia.etherscan.io/address/0xa626578A62824dD4b3259424c329F764F2E08965#code

# Use

To generate random `bytes32` identifiers needed for creating an order, first run the Hardhat console:
```
npx hardhat console
```
And then inside the console run:
```
ethers.utils.hexlify(ethers.utils.randomBytes(32))
```
Example output:
```
0xea4372bafdb505d23d0bc115563c6dd89d017484dec4a0ee49d7d5f2f03dd1f5
0x115b5b138239bed22f1e407ed5fc5e755e12618c3be5e886a6b96805c76fd6f1
0xee154209048d04de26f6aebdfa9dd0f103b179cb5c5c5481de559490f32e6fa8
```