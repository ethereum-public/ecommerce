import { HardhatUserConfig } from "hardhat/config";
import "@nomicfoundation/hardhat-toolbox";
import * as dotenv from "dotenv";
import "./tasks";

dotenv.config();
const config: HardhatUserConfig = {
  solidity: {
    version: "0.8.19",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200,
      },
    },
  },
  etherscan: {
    apiKey: process.env["ETHERSCAN_API_KEY"],
  },
  networks: {
    sepolia: {
      url: `https://sepolia.infura.io/v3/${process.env["INFURA_API_KEY"]}`,
      accounts: [ process.env["PRIVATE_KEY"] as string ],
    },
  },
};

export default config;